# NXT ShapeShift #

* Quickly and simply exchange NXT with other cryptocurrency without registration
* Version 1.0

### Prerequisites ###

* NXT 1.5.14 or higher (the plugin won't work with earlier versions)

### Setup ###

* Download shape_shift.zip only from https://bitbucket.org
* Verify the sha256 hash of the zip
* Unzip shape_shift.zip into your <NXTRoot>\html\ui\plugins folder
* Login to your NXT account and in the settings menu choose "Enable Plugins" = "Yes"
* Logout and login again
* The "Shape Shift" plugin should appear under the "Plugins" menu

### Usage ###

* For each currency pair you can choose one of the following actions "shift" and "pay" which correspond to the "shift" and "sendamount" shapeshift APIs
* Shift - you specify the amount in the deposit currency
* Pay - you specify the amount in the withdrawal currency and shapeshift returns the deposit amount in the deposit currency (this option provides better conversion rate)
* Exchanges are stored locally in the browser cache, therefore when switching a browser or client workstation you will no longer see your exchange history

### Security ###

* As usual with the NXT wallet your passphrase never leaves your local workstation, it is only used as a parameter to the standard SendMoney API
* All other information you enter is shared only with https://shapeshift.io
* shift and sendamount transactions are submitted using the plugin api public key, this enables the plugin developer to diagnose problems without relying on shapeshift itself. This should not compromise your security or anonymity

### Support ###

* This plugin comes with absolutely no warranty
* Post to https://nxtforum.org/nxt-plugins/shapeshift-integration-plugin/
* Contact lyaffe@gmail.com

### Limitations ###

* For funding your account for the first time (account without public key) you have to use the "shift" operation since "pay" does not work
* General

### One More Thing ###

* If you like my work, show it: NXT-62SZ-7MMM-CUDQ-8R5W8